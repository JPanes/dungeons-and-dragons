/**
 * Where the user interaction occurs
 * @author Joe Panes
 */
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.Random;

public class Main {
    private static final int MAX = 9999;
    private static final int MIN = 0;

    /**
     * The main class where all of the user interaction will be
     */
    public static void main(String[] args) throws FileNotFoundException  {
        //Windows file path
        ///String filename = "D:\\DNDProgram\\dungeons-and-dragons\\tables\\WildMagicEffects.txt";
        //Mac file path
        String filename = "/Users/a.m./Documents/DND/dungeons-and-dragons/tables/WildMagicEffects.txt";
        int wildNo = 1;
        Scanner input = new Scanner(System.in);

        //Introductory message, and description of interaction with program
        System.out.println("Welcome to my D&D program.");
        System.out.println("You can enter the following:");
        System.out.println("(W)ild magic");
        String in = input.nextLine();


        //How the user will interact with the program
        if (in.equalsIgnoreCase("W")) {
            //Allow more than once, without having to restarting program
            boolean goBack = false;
            do {

                //Wild magic effect generator
                System.out.println("Do you want a (R)andom number, to (S)elect a number or go (B)ack?");
                in = input.next();

                if (in.equalsIgnoreCase("R")) {
                    //Generates random number
                    Random random = new Random();

                    wildNo = random.ints(MIN,(MAX)+1).findFirst().getAsInt();

                    System.out.println (getWildMagic(filename,wildNo));





                }else if (in.equalsIgnoreCase("S")) {
                    boolean correctInput;

                    //Gets the number from the user
                    do {
                        correctInput = true;

                        //Make sure program won't break if a string is entered
                        try {
                            System.out.print("Input number: ");
                            wildNo = input.nextInt();

                        } catch(ArithmeticException e) {
                            input.nextLine();
                            System.out.println("Please only enter numbers between 0 and 9999");
                            correctInput = false;
                        }


                    }while(wildNo <0 || wildNo > 9999  || correctInput == false);

                System.out.println (getWildMagic(filename,wildNo));

                }else if (in.equalsIgnoreCase("B")) {
                    //Exit loop
                    goBack = true;
                }
            }while(goBack == false);

            input.close();
        }
    }

    /**
     * Gets the wild magic effect
     * @param filename The directory address of the wild magic table
     * @param wildNo The number of the magical effect to be found
     * @return The magical effect
     */
    public static String getWildMagic (String filename, int wildNo) {
        try {
            return wildNo + ":" + WildMagic.readFile(filename, wildNo).getWildMagicEffect();
        }catch (FileNotFoundException e) {
            return "I can't find the wild magic table";
        }
    }
}
