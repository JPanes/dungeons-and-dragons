/**
 * Handles all the methods regarding the wild magic text filename
 * @author Joe Panes
 */

import java.util.Scanner;
import java.io.*;
public class WildMagic {
    private int wildMagicNo;
    private String wildMagicEffect;

    /**
     * Looks at each line of the text file from the top down
     * If it is the wild magic effect it is looking for it will take the string and end
     * Otherwise, it will move on to the next and repeat until it is found
     * @param in The text file of wild magic table
     * @param wildNo The number of the spell it is looking for
     * @return Wild magic effect and number
     */
    private static WildMagic readDataFile(Scanner in, int wildNo) {
        //Initialising a wild magic object for result
        WildMagic effect = new WildMagic();
        boolean foundWildNo = false;

        effect.setWildMagicNo(wildNo);

        while (in.hasNextLine() && in.hasNext() && foundWildNo == false) {
        	//Set up to prevent errors from checking next line
         	String curLine = in.nextLine();
         	Scanner line = new Scanner(curLine);

            //Find out if this is the wild magic effect
            if (effect.getWildMagicNo() == line.nextInt()) {

                foundWildNo = true;
                String stEffect = "";

                do {
                    stEffect = line.nextLine();
                } while (line.hasNext());

                effect.setWildMagicEffect(stEffect);
            }
            line.close();
        }

        in.close();
		return effect;
	}

    /**
     * @param filename The directory address of the text file
     * @param wildNo The number of the spell it is looking for
     * @return Wild magic effect and number
     */
    public static WildMagic readFile(String filename, int wildNo) throws FileNotFoundException {
        // Initialising scanner
        Scanner in = null;
		 try {
             //Setting up scanner so that if file name is incorrect, program won't break
             in = new Scanner(new File(filename));

         }catch (FileNotFoundException e) {
             throw new FileNotFoundException  ("The wild magic text file cannot be found, or has been renamed.");
         }
		 //Send scanner to next phase of reading
         return WildMagic.readDataFile(in,wildNo);
	}

	/**
	 * Returns value of wildMagicNo
	 * @return The Wild magic number to be found
	 */
	public int getWildMagicNo() {
	       return wildMagicNo;
	}

	/**
	 * Sets new value of wildMagicNo
	 * @param wildMagicNo The number of the spell
	 */
	public void setWildMagicNo(int wildMagicNo) {
	       this.wildMagicNo = wildMagicNo;
	}

	/**
	 * Returns value of wildMagicEffect
	 * @return The effect of the spell
	 */
	public String getWildMagicEffect() {
	       return wildMagicEffect;
	}

	/**
	 * Sets new value of wildMagicEffect
	 * @param wildMagicEffect The effect of the spell
	 */
	public void setWildMagicEffect(String wildMagicEffect) {
	       this.wildMagicEffect = wildMagicEffect;
	}
}
